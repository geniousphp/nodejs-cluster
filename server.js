var cluster = require('cluster');

if (cluster.isMaster) {

    var cpuCount = require('os').cpus().length;
    // console.log(cpuCount);
    // console.log('Master is forking!');
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

	cluster.on('exit', function (worker) {
	    // console.log('Worker ' + worker.id + ' died :(');
	    cluster.fork();
	});

} 
else {
    var express = require('express');
    var app = express();

    app.get('/', function (req, res) {
        console.log("request");
        res.send('Hello world');
        // res.send('Hello from Worker ' + cluster.worker.id);

    });

    app.listen(3000);
	// console.log('Worker ' + cluster.worker.id + ' running!');
}
