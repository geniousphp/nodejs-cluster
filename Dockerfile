FROM node
ADD start.sh /tmp/
EXPOSE  3000
RUN chmod +x /tmp/start.sh
CMD /tmp/start.sh